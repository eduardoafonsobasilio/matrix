import unittest
import numpy
from scipy.sparse import isspmatrix_csr, isspmatrix_csc, isspmatrix_lil

from matrix import SparseMatrix


class TestSparseMatrix(unittest.TestCase):
	def setUp(self):
		self.array = [[0, 0, 1, 3, 5, 7, -9], [0, 2, 4, 6, 0, -8, 10]]
		self.sm = SparseMatrix(self.array)

	def test_intern_represent(self):
		print('Q1: Test if intern_represent is CSR')
		self.assertEqual('CSR', self.sm.intern_represent)

	def test_is_csr(self):
		print('Q1: Test if type matrix is CSR')
		self.assertEqual(True, isspmatrix_csr(self.sm.matrix))

	def test_nonzero(self):
		print('Q2: Test number of nonzero elements')
		self.assertEqual(8, self.sm.nonzero)

	def test_cell_update(self):
		print('Q3: Test cell update value')
		self.sm.cell_update(1, 0, 33)
		self.assertEqual(self.sm.matrix[1, 0], 33)
		
		print('Q3: Test update nonzero elements after cell update')
		self.assertEqual(9, self.sm.nonzero)

	def test_csr_to_csc(self):
		print('Q4: Test CSR to CSC')
		self.sm.to_csc()
		self.assertEqual(True, isspmatrix_csc(self.sm.matrix))

		print('Q4: Test update intern_represent after change CSR Matrix to CSC Matrix')
		self.assertEqual('CSC', self.sm.intern_represent)

	def test_csr_to_lil(self):
		'''
		Test CSR to LIL
		'''
		self.sm.to_lil()
		self.assertEqual(True, isspmatrix_lil(self.sm.matrix))

		# Test update intern_represent after change CSR Matrix to LIL Matrix
		self.assertEqual('LIL', self.sm.intern_represent)

	def test_is_equal(self):
		print('Q5: Test if two matrix are equal')
		equal_sm = self.sm
		self.assertEqual(True, self.sm.is_equal(equal_sm.matrix))
		
		different_sm = SparseMatrix(self.array)
		different_sm = different_sm.cell_update(1, 0, 500)
		self.assertEqual(False, self.sm.is_equal(different_sm.matrix))

	def test_tol_convert(self):
		print('Q6: Convert elements less than tol to zero')
		array = [[-1, 0, 1], [-1, 0, 1]]
		sm = SparseMatrix(array)
		self.assertEqual(2, sm.nonzero)
		sm.tol_convert(2)
		self.assertEqual(0, sm.nonzero)

	def test_elementwise_add(self):
		print('Q7: Test element-wise addition of two objects SparseMatrix')
		array = [1, 2, 3]
		sm1 = SparseMatrix(array)
		sm2 = SparseMatrix(array)
		
		result = sm1.elementwise_add(sm2)
		
		self.assertEqual(2,		result.matrix[0, 0])
		self.assertEqual(4,		result.matrix[0, 1])
		self.assertEqual(6,		result.matrix[0, 2])



	def test_multiplies_with_numpy_array(self):
		print('Q8: Test multiplies SparseMatrix with numpy.array')
		array = [[1, 2, 3], [4, 5, 6]]
		sm = SparseMatrix(array)

		result = sm.multiply(numpy.array([2, 4, 6]))
		
		self.assertEqual(2,		result.toarray()[0, 0])
		self.assertEqual(8,		result.toarray()[0, 1])
		self.assertEqual(18,	result.toarray()[0, 2])
		self.assertEqual(8,		result.toarray()[1, 0])
		self.assertEqual(20, 	result.toarray()[1, 1])
		self.assertEqual(36,	result.toarray()[1, 2])


if __name__ == '__main__':
	unittest.main()
