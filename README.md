# README #
Sparser Matrices academic work

### Install [pip](https://pip.pypa.io/en/stable/) ###
In terminal linux do:

```sudo apt install python-pip```

### Install [pipenv](http://pipenv.readthedocs.io) ###
pipenv creates and manages a virtualenv for your projects.

In terminal linux do:

```pip install pipenv```

_Now, download this directory, unziped and rename to **matrix**._

# With terminal linux in **matrix directory** do: #

### Install dependences of work ###

`pipenv install`

### Run academic work ###

`pipenv run python matrix.py`

### Run tests ###

`pipenv run python tests.py`