import numpy
from scipy import sparse


class SparseMatrix:
	matrix = 'None'
	# csr_matrix = None
	intern_represent = 'numpy.ndarray'

	@property
	def nonzero(self):
		'''
		Return number of non-zero entries
		'''
		return self.matrix.count_nonzero()
	
	def __init__(self, array):
		self.matrix = numpy.array(array)
		self.to_csr()
		self.tol_convert()
	
	def tol_convert(self, tol=0):
		'''
		Receive tol parameter
		Convert elements < tol in zero
		'''
		self.matrix.data = numpy.where(self.matrix.data < tol, 0, self.matrix.data)
		return self.matrix.toarray()

	def multiply(self, other):
		'''
		Point-wise multiplication by another matrix, vector, or scalar.
		'''
		return self.matrix.multiply(other)

	def elementwise_add(self, other):
		'''
		Receive a SparseMatrix obj
		Return a SparseMAtrix obj element-wise add
		'''
		result = numpy.add(self.matrix.toarray(), other.matrix.toarray())
		result = SparseMatrix(result.tolist())
		
		return result


	def to_csr(self):
		'''
		Convert self.matrix to CSR matrix
		'''
		self.matrix = sparse.csr_matrix(self.matrix)
		self.intern_represent = 'CSR'
		return self.matrix

	def to_csc(self):
		'''
		Convert self.matrix to CSC matrix
		'''
		self.matrix = self.matrix.tocsc()
		self.intern_represent = 'CSC'
		return self.matrix

	def to_lil(self):
		'''
		Convert self.matrix to LIL matrix
		'''
		self.matrix = self.matrix.tolil()
		self.intern_represent = 'LIL'
		return self.matrix

	def cell_update(self, col, row, value):
		'''
		Update the value of a cell in matrix
		Receiver parameters col, row and value
		Return new value of cell 
		'''
		self.to_lil() # to avoid a SparseEfficiencyWarning: lil_matrix is more efficient
		self.matrix[col, row] = value
		self.to_csr()
		self.tol_convert()
		return self


	def is_equal(self, matrix):
		'''
		Receive matrix and compare if is equal
		'''
		return True if self.matrix is matrix else False


def main():
	array = [[0, 0, 1, 3, 5, 7, -9], [0, 2, 4, 6, 0, -8, 10]]
	print('array: ', array)
	
	print('::::::: QUESTION 1 ::::::')
	print('Create obj sm of type SparseMatrix(array)')
	sm = SparseMatrix(array=array)

	print('sm.matrix')
	print(sm.matrix)
	print('Note: values less than 0 were converted to 0')

	print('type(sm.matrix):', type(sm.matrix))
	
	print('sm.intern_represent:', sm.intern_represent)

	print('::::::: QUESTION 2 ::::::')
	print('sm.nonzero:', sm.nonzero)

	print('::::::: QUESTION 3 ::::::')
	print('Update sm.matrix[1, 0] to 33:')
	sm.cell_update(1, 0, 33)
	print('sm.matrix[1, 0]:', sm.matrix[1, 0])
	print('sm.nonzero:', sm.nonzero)

	print('::::::: QUESTION 4 ::::::')
	print('sm.to_csc()')
	print(sm.to_csc())
	print('type(sm.matrix):', type(sm.matrix))
	print('sm.intern_represent:', sm.intern_represent)

	print('::::::: QUESTION 5 ::::::')
	print('sm.is_equal(sm.matrix):', sm.is_equal(sm.matrix))

	print('::::::: QUESTION 6 ::::::')
	print('sm.cell_update(1, 0, -33):')
	sm.cell_update(1, 0, -33)
	print('sm.matrix[1, 0]:', sm.matrix[1, 0])

	print('::::::: QUESTION 7 ::::::')
	array = [1, 2, 3]
	print('array: ', array)
	sm1 = SparseMatrix(array)
	sm2 = SparseMatrix(array)
	print('sm1 = SparseMatrix(array)')
	print('sm2 = SparseMatrix(array)')

	result = sm1.elementwise_add(sm2)
	print('result = sm1.elementwise_add(sm2)')
	print(result.matrix)

	print('::::::: QUESTION 8 ::::::')
	array = [[1, 2, 3], [4, 5, 6]]
	print('array: ', array)

	sm = SparseMatrix(array)
	print('sm = SparseMatrix(array)')

	result = sm.multiply(numpy.array([2, 4, 6]))
	print('result = sm.multiply(numpy.array([2, 4, 6]))')
	print(result)

	print('::::::: QUESTION 9 ::::::')
	print('run tests.py')


if __name__ == '__main__':
	main()
